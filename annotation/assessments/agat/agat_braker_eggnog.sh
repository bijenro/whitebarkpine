#!/bin/bash
#SBATCH --job-name=agat
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

source activate agat 

agat_sp_statistics.pl --gff augustus_eggnog_filtered.gff3 




echo -e "\nEnd time:"
date

