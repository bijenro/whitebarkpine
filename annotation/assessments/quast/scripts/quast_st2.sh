#!/bin/bash
#SBATCH --job-name=quast_st2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load quast
quast.py -v
quast.py -o st2_only /core/labs/Wegrzyn/whitebarkpine/stringtie/stringtie2/frameselect/stringtie2_hybrid_allshort_transcripts.fa

echo -e "\nEnd time:"
date

