#!/bin/bash
#SBATCH --job-name=eggnog_braker
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load sqlite
#module load hmmer/3.3.2
#module load perl/5.24.0
#module load TransDecoder/5.5.0
#module load blast/2.11.0

source activate eggnog
filename=combined_braker_stringtie_protein.fa 
emapper.py -i $filename -o braker.eggnog.blastp -m diamond --cpu 16




echo -e "\nEnd time:"
date

