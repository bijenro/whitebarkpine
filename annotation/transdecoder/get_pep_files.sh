#!/bin/bash
#SBATCH --job-name=get_pep_files
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load seqtk 
grep ">" /core/labs/Wegrzyn/whitebarkpine/stringtie/transdecoder_genome/stringtie_hybrid.fa > stringtie_hybrid_tnames.txt
sed -i "s/>//g" stringtie_hybrid_tnames.txt
seqtk subseq /core/labs/Wegrzyn/whitebarkpine/stringtie/stringtie2/frameselect/stringtie2_hybrid_allshort_transcripts.fa.transdecoder.pep stringtie_hybrid_tnames.txt > stringtie_hybrid.pep.fa 

grep ">" /core/labs/Wegrzyn/whitebarkpine/stringtie/transdecoder_genome/stringtie_short.fa > stringtie_short_tnames.txt 
sed -i "s/>//g" stringtie_short_tnames.txt
seqtk subseq /core/labs/Wegrzyn/whitebarkpine/stringtie/stringtie1/frameselect/stringtie_merge_transcripts.fa.transdecoder.pep stringtie_short_tnames.txt > stringtie_short.pep.fa


echo -e "\nEnd time:"
date

