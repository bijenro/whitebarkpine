#!/bin/bash
#SBATCH --job-name=busco_after_eggnog
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=30G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load busco/5.2.2
module load MetaEuk/4.0
module load hmmer/3.3.2

busco -c 8 -i /core/labs/Wegrzyn/whitebarkpine/stringtie/frameselect/stringtie_merge_transcripts.fa.transdecoder.pep -l embryophyta_odb10 -o busco_after_eggnog -m proteins 



echo -e "\nEnd time:"
date

