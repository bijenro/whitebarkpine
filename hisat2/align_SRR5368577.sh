#!/bin/bash
#SBATCH --job-name=hisatalign_SRR5368577
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load hisat2
module load samtools

mkdir -p alignments
hisat2 -p 12 -x wbp_3kb_filt --max-intronlen 2500000 -1 ../fastp/trimmed_SRR5368577_1.fastq.gz -2 ../fastp/trimmed_SRR5368577_2.fastq.gz -S SRR5368577.sam
samtools view -b -@ 12 SRR5368577.sam | samtools sort -o sorted_SRR5368577.bam -@ 12
rm SRR5368577.sam

date
