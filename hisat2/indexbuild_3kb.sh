#!/bin/bash
#SBATCH --job-name=indexbuild_3kb 
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load hisat2/2.2.1

hisat2-build -p 12 ../data/wbp_3kb_filtered.fa wbp_3kb_filt 


echo -e "\nEnd time:"
date

