#!/bin/bash
#SBATCH --job-name=repeatmodel_highermem
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=300G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date


module unload perl/5.28.0
module load perl/5.24.0
export PERL5LIB=/UCHC/PublicShare/szaman/perl5/lib/perl5/
module load RepeatModeler/2.01
module load genometools/1.6.1
module load mafft/7.471
export LTRRETRIEVER_PATH=/core/labs/Wegrzyn/annotationtool/software/LTR_retriever
module load cdhit/4.8.1
module load ninja/0.95
export TMPDIR=/core/labs/Wegrzyn/whitebarkpine/

BuildDatabase -name wbp_highermem /core/labs/Wegrzyn/whitebarkpine/data/wbp_3kb_filtered.fa
RepeatModeler -database wbp_highermem -pa 12 -LTRStruct

echo -e "\nEnd time:"
date


